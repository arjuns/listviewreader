#define WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <windows.h>
#include <commctrl.h>

int main(void) {
	/*HWND hwnd=FindWindow(NULL, "Stealing Program's Memory: ListView");
	HWND listview=FindWindowEx(hwnd, NULL, "SysListView32", NULL);*/

	//HWND hwnd=FindWindow(NULL, "Stealing Program's Memory: ListView");
	HWND listview=(HWND)0x00050AC6;

	int count=(int)SendMessage(listview, LVM_GETITEMCOUNT, 0, 0);
	int i;

	LVITEM lvi, *_lvi;
	WCHAR item[512], subitem[512];
	WCHAR *_item, *_subitem;
	unsigned long pid;
	HANDLE process;

	GetWindowThreadProcessId(listview, &pid);
	process=OpenProcess(PROCESS_VM_OPERATION|PROCESS_VM_READ|PROCESS_VM_WRITE|PROCESS_QUERY_INFORMATION, FALSE, pid);

	_lvi=(LVITEM*)VirtualAllocEx(process, NULL, sizeof(LVITEM), MEM_COMMIT, PAGE_READWRITE);
	_item=(WCHAR*)VirtualAllocEx(process, NULL, 512, MEM_COMMIT, PAGE_READWRITE);
	_subitem=(WCHAR*)VirtualAllocEx(process, NULL, 512, MEM_COMMIT, PAGE_READWRITE);

	lvi.cchTextMax=512;

	for(i=0; i<count; i++) {
		lvi.iSubItem=0;
		lvi.pszText=_item;
		WriteProcessMemory(process, _lvi, &lvi, sizeof(LVITEM), NULL);
		SendMessage(listview, LVM_GETITEMTEXT, (WPARAM)i, (LPARAM)_lvi);

		lvi.iSubItem=1;
		lvi.pszText=_subitem;
		WriteProcessMemory(process, _lvi, &lvi, sizeof(LVITEM), NULL);
		SendMessage(listview, LVM_GETITEMTEXT, (WPARAM)i, (LPARAM)_lvi);

		ReadProcessMemory(process, _item, item, 512, NULL);
		ReadProcessMemory(process, _subitem, subitem, 512, NULL);

		printf("%s - %s\n", item, subitem);
	}

	VirtualFreeEx(process, _lvi, 0, MEM_RELEASE);
	VirtualFreeEx(process, _item, 0, MEM_RELEASE);
	VirtualFreeEx(process, _subitem, 0, MEM_RELEASE);

	return 0;
}
