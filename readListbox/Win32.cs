﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace readListbox
{


    public class WindowFinder
    {

        public static string GetListViewItem(IntPtr hwnd, uint processId, int item, int subItem = 0)
        {
            const int dwBufferSize = 2048;
            const int LVM_FIRST = 0x1000;
            const int LVM_GETITEMW = LVM_FIRST + 75;
            const int LVIF_TEXT = 0x00000001;

            int bytesWrittenOrRead = 0;
            LV_ITEM lvItem;
            string retval;
            bool bSuccess;
            IntPtr hProcess = IntPtr.Zero;
            IntPtr lpRemoteBuffer = IntPtr.Zero;
            IntPtr lpLocalBuffer = IntPtr.Zero;

            try
            {
                lvItem = new LV_ITEM();
                lpLocalBuffer = Marshal.AllocHGlobal(dwBufferSize);
                hProcess = NativeApi.OpenProcess(Win32ProcessAccessType.AllAccess, false, processId);
                if (hProcess == IntPtr.Zero)
                    throw new ApplicationException("Failed to access process!");

                lpRemoteBuffer = NativeApi.VirtualAllocEx(hProcess, IntPtr.Zero, dwBufferSize, Win32AllocationTypes.MEM_COMMIT, Win32MemoryProtection.PAGE_READWRITE);
                if (lpRemoteBuffer == IntPtr.Zero)
                    throw new SystemException("Failed to allocate memory in remote process");

                lvItem.mask = LVIF_TEXT;
                lvItem.iItem = item;
                lvItem.iSubItem = subItem;
                unsafe
                {
                    lvItem.pszText = (char*)(lpRemoteBuffer.ToInt32() + Marshal.SizeOf(typeof(LV_ITEM)));
                }
                lvItem.cchTextMax = 500;

                bSuccess = NativeApi.WriteProcessMemory(hProcess, lpRemoteBuffer, ref lvItem, (uint)Marshal.SizeOf(typeof(LV_ITEM)), out bytesWrittenOrRead);
                if (!bSuccess)
                    throw new SystemException("Failed to write to process memory");

                NativeApi.SendMessage(hwnd, LVM_GETITEMW, IntPtr.Zero, lpRemoteBuffer);

                bSuccess = NativeApi.ReadProcessMemory(hProcess, lpRemoteBuffer, lpLocalBuffer, dwBufferSize, out bytesWrittenOrRead);

                if (!bSuccess)
                    throw new SystemException("Failed to read from process memory");

                retval = Marshal.PtrToStringUni((IntPtr)(lpLocalBuffer.ToInt32() + Marshal.SizeOf(typeof(LV_ITEM))));
            }
            finally
            {
                if (lpLocalBuffer != IntPtr.Zero)
                    Marshal.FreeHGlobal(lpLocalBuffer);
                if (lpRemoteBuffer != IntPtr.Zero)
                    NativeApi.VirtualFreeEx(hProcess, lpRemoteBuffer, 0, Win32AllocationTypes.MEM_RELEASE);
                if (hProcess != IntPtr.Zero)
                    NativeApi.CloseHandle(hProcess);
            }
            return retval;
        }


        public static IntPtr FindWindowInProcess(Process process, Func<string, bool> compareTitle)
        {
            IntPtr windowHandle = IntPtr.Zero;

            foreach (ProcessThread t in process.Threads)
            {
                windowHandle = FindWindowInThread(t.Id, compareTitle);
                if (windowHandle != IntPtr.Zero)
                {
                    break;
                }
            }

            return windowHandle;
        }

        private static IntPtr FindWindowInThread(int threadId, Func<string, bool> compareTitle)
        {
            IntPtr windowHandle = IntPtr.Zero;
            NativeApi.EnumThreadWindows(threadId, (hWnd, lParam) =>
            {
                StringBuilder text = new StringBuilder(200);
                NativeApi.GetWindowText(hWnd, text, 200);
                if (compareTitle(text.ToString()))
                {
                    windowHandle = hWnd;
                    return false;
                }
                return true;
            }, IntPtr.Zero);

            return windowHandle;
        }


        /// <summary>
        /// Returns a list of child windows
        /// </summary>
        /// <param name="parent">Parent of the windows to return</param>
        /// <returns>List of child windows</returns>
        public static List<IntPtr> GetChildWindows(IntPtr parent)
        {
            List<IntPtr> result = new List<IntPtr>();
            GCHandle listHandle = GCHandle.Alloc(result);
            try
            {
                EnumWindowProc childProc = new EnumWindowProc(EnumWindow);
                NativeApi.EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
            }
            finally
            {
                if (listHandle.IsAllocated)
                    listHandle.Free();
            }
            return result;
        }

        /// <summary>
        /// Callback method to be used when enumerating windows.
        /// </summary>
        /// <param name="handle">Handle of the next window</param>
        /// <param name="pointer">Pointer to a GCHandle that holds a reference to the list to fill</param>
        /// <returns>True to continue the enumeration, false to bail</returns>
        private static bool EnumWindow(IntPtr handle, IntPtr pointer)
        {
            GCHandle gch = GCHandle.FromIntPtr(pointer);
            List<IntPtr> list = gch.Target as List<IntPtr>;
            if (list == null)
            {
                throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");
            }
            list.Add(handle);
            //  You can modify this to check to see if you want to cancel the operation, then return a null here
            return true;
        }

        /// <summary>
        /// Delegate for the EnumChildWindows method
        /// </summary>
        /// <param name="hWnd">Window handle</param>
        /// <param name="parameter">Caller-defined variable; we use it for a pointer to our list</param>
        /// <returns>True to continue enumerating, false to bail.</returns>
        public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        public static string GetWindowClassName(IntPtr windowHandle)
        {
            StringBuilder sb = new StringBuilder(513);
            var x = GetClassName(windowHandle, sb, 512);
            return sb.ToString();
        }

        public static string GetWindowText(IntPtr winHandle)
        {
            StringBuilder sb = new StringBuilder(1024);
            NativeApi.GetWindowText(winHandle, sb, 1024);
            return sb.ToString();
        }
        public static int GetListViewItemCount(IntPtr listViewHandle)
        {
            return (int)NativeApi.SendMessage(listViewHandle, (uint)NativeApi.LVM_GETITEMCOUNT, IntPtr.Zero, IntPtr.Zero);
        }
    }

    public class ListViewItemCopier
    {
        private readonly int processId;
        private IntPtr mainWindowHandle;

        public ListViewItemCopier()
        {
            Process processById = Process.GetProcesses().FirstOrDefault(x => x.MainWindowTitle.StartsWith("KVS Availability Tool"));
            if (processById == null)
                throw new ListViewCopierException("Looks Like program is not running.");
            processId = processById.Id;
            mainWindowHandle = WindowFinder.FindWindowInProcess(processById, x => x.StartsWith("KVS Availability Tool "));

        }

        protected IntPtr GetListViewWindowHandle()
        {

            List<IntPtr> childWindows = WindowFinder.GetChildWindows(mainWindowHandle);
            var ax = childWindows.Where(x => WindowFinder.GetWindowClassName(x) == "ListViewWndClass").ToList();
            return ax.LastOrDefault();

        }
        protected string ReadItem(IntPtr listViewHandle, int index, int subIndex)
        {

            return WindowFinder.GetListViewItem(listViewHandle, (uint)processId, index, subIndex);
        }
        public List<AvaliabilityDetail> GetAllAvailability()
        {
            var listViewHandle = GetListViewWindowHandle();
            int totalitemCount = WindowFinder.GetListViewItemCount(listViewHandle);
            List<AvaliabilityDetail> result = new List<AvaliabilityDetail>();

            for (int i = 0; i < totalitemCount; i++)
            {
                AvaliabilityDetail detail = new AvaliabilityDetail();

                detail.Carrier = ReadItem(listViewHandle, i, 0);
                detail.Flight = ReadItem(listViewHandle, i, 1);
                detail.From = ReadItem(listViewHandle, i, 2);
                detail.Depart = ReadItem(listViewHandle, i, 3);
                detail.To = ReadItem(listViewHandle, i, 4);
                detail.Arrive = ReadItem(listViewHandle, i, 5);
                detail.AC = ReadItem(listViewHandle, i, 6);
                detail.St = ReadItem(listViewHandle, i, 7);
                detail.Availability = ReadItem(listViewHandle, i, 8);
                result.Add(detail);

            }
            return result;
        }
    }

    public class ListViewCopierException : Exception
    {
        public ListViewCopierException(string msg)
            : base(msg)
        {

        }
    }
}