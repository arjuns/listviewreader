using System.Reflection;
using System.Text;

namespace readListbox
{
    public class AvaliabilityDetail
    {
        public string Carrier { get; set; }
        public string Flight { get; set; }
        public string From { get; set; }
        public string Depart { get; set; }
        public string To { get; set; }
        public string Arrive { get; set; }
        public string AC { get; set; }
        public string St { get; set; }
        public string Availability { get; set; }

        public override string ToString()
        {
            var prop = this.GetType().GetProperties();
            StringBuilder sb=new StringBuilder();
            foreach (var propertyInfo in prop)
            {
                sb.AppendLine(string.Format("{0}   -          {1}", propertyInfo.Name, propertyInfo.GetValue(this,null)));
            }
            return sb.ToString();
        }
    }
}