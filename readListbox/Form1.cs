﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace readListbox
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Process processById = Process.GetProcesses().FirstOrDefault(x => x.MainWindowTitle.StartsWith("KVS Availability Tool"));
            if(processById==null)
            {
                
            }
        }

        private void ReadClicked(object sender, EventArgs e)
        {
            try
            {
                ListViewItemCopier copier = new ListViewItemCopier();
                var avaliabilityDetails = copier.GetAllAvailability();
                StringBuilder sb = new StringBuilder();
                foreach (var details in avaliabilityDetails)
                {
                    sb.AppendLine(details.ToString());
                }
                this.textBox1.Text = sb.ToString();
            }
            catch(ListViewCopierException cpt)
            {
                MessageBox.Show("Looks like KVS availability tool is not running! \r please make sure it is running.");
            }
        }
    }
}
